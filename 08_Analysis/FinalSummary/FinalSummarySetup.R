# To cite this work in publications, please use:
#     Deliz-Aguirre, Cao, et al. (2020) IRAK4 controls MyD88 oligomer size to regulate Myddosome assembly


# Copyright 2020 (c) Rafael Deliz-Aguirre
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the 'Software'), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# For merging tables

print(":::::::::::::::::::: START GRAND SUMMARY LOOP ::::::::::::::::::::")
tic()

#Prepare GrandTableSpots
#A table containing all pucta data
LOCAL_DIRECTORY <- getwd()
setwd(FINALSUMMARY_SCRIPTS)
source("GrandTableSpots.R", local = T)

#Prepare GrandTableTracks
#A table containing summarized puncta data
LOCAL_DIRECTORY <- getwd()
setwd(FINALSUMMARY_SCRIPTS)
source("GrandTableTracks.R", local = T)

#Error table
#For cells that were included in input,
#but that don't have an analysis output
LOCAL_DIRECTORY <- getwd()
setwd(FINALSUMMARY_SCRIPTS)
source("ErrorTable.R", local = T)

print(":::::::::::::::::::: END GRAND SUMMARY LOOP ::::::::::::::::::::")
toc()